var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/*  */
router.get('/test', function(req, res, next) {
  res.send("Test new route");
});

router.get('/hello', function(req, res, next) {
  res.send("Hello world");
});


module.exports = router;
